let modal = document.createElement('div');
let closeIcon = document.createElement('img');

let catalogList = [
    'Общестроительные материалы',
    'Сухие строительные смеси и гидроизоляция',
    'Теплоизоляция и шумоизоляция',
    'Материалы для сухого строительства',
    'Древесно-плитные материалы',
    'Пиломатериалы',
    'Электроинструмент и комплектующи',
    'Кровля, сайдинг, водосточные системы',
    'Лакокрасочные материалы',
    'Пены, клеи, герметики',
    'Керамическая плитка и затирки',
    'Финишная отделка стен и потолков',
    'Напольные покрытия',
    'Двери, окна, скобяные изделия',
    'Инженерная сантехника',
    'Отопительное и насосное оборудование'

];
console.log(catalogList);

let ulSidebar = document.querySelector('.menu') //получили доступ к элементу с классом 'menu'

for (let i = 0; i < catalogList.length; i++) {

    let li = document.createElement('li'); // создали тег <li> - он пока не существует в DOM-дерево
    let listItem = document.createElement('a'); // создали тег <a> - он пока не добавлен в DOM-дерево

    ulSidebar.appendChild(li) //добавляем элементу с классом 'menu' тег <li> - появляется в DOM -дереве
    li.appendChild(listItem) //вложили внутри <li> тег <a>  - появляется в DOM -дереве
    listItem.setAttribute('class', 'list-menu') //добавляем тегу <a> атрибут  'class' со значением 'list-menu'
    listItem.innerHTML = catalogList[i] // Добавляем i-тому элементу цикла i-тое значение массива списка
}


const onModal = () => {
    document.body.appendChild(modal);
    modal.className = "modal-wrapper";
    modal.setAttribute('style', 'display: flex; justify-content: flex-end; align-items: flex-start;')
    modal.appendChild(closeIcon)
    closeIcon.setAttribute('src', './assets/icons/close.svg')
    closeIcon.setAttribute('style', 'width: 24px; margin: 250px; cursor: pointer')
    closeIcon.setAttribute('onclick', 'onCloseModal()')
}

const onCloseModal = () => {
    modal.remove.className = "modal-wrapper"
    modal.className = "close-modal";
}

document.querySelector('.span_color').addEventListener('click', onModal)